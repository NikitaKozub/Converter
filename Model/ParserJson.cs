﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Парсер JSON
    /// </summary>
    public class ParserJson
    {
        /// <summary>
        /// Парсит файл формата json
        /// </summary>
        /// <returns></returns>
        public DailyValutes GetData()
        {
            WebClient c = new WebClient();
            var vLogin = c.DownloadString("https://www.cbr-xml-daily.ru/daily_json.js");
            DailyValutes dailyValutes = JsonConvert.DeserializeObject<DailyValutes>(vLogin);
            AddRub(dailyValutes);
            return dailyValutes;
        }

        /// <summary>
        /// Добавление рубля
        /// </summary>
        /// <param name="dailyValutes">объект с набором валют</param>
        private void AddRub(DailyValutes dailyValutes)
        {
            Valute rub = new Valute();
            rub.Nominal = 1;
            rub.Value = 1;
            rub.Name = "Российский рубль";
            rub.CharCode = "RUB";
            dailyValutes.Valute.Add("RUB", rub);
        }
    }

    /// <summary>
    /// Внутренний класс, ничего не делает, просто для хранения значений при парсинге
    /// </summary>
    public class DailyValutes
    {
        /// <summary>
        /// Дата получения курса
        /// </summary>
        [JsonProperty("Date")]
        public DateTime Date;
        [JsonProperty("PreviousDate")]
        public DateTime PreviousDate;
        [JsonProperty("PreviousURL")]
        public string PreviousURL;
        [JsonProperty("Timestamp")]
        public string Timestamp;
        /// <summary>
        /// Список валют
        /// </summary>
        [JsonProperty("Valute")]
        public Dictionary<string, Valute> Valute { get; set; }
    }

    /// <summary>
    /// Внутренний класс, ничего не делает, просто для хранения значений при парсинге
    /// </summary>
    public class Valute
    {
        [JsonProperty("ID")]
        public string ID;
        [JsonProperty("NumCode")]
        public string NumCode;
        [JsonProperty("CharCode")]
        /// <summary>
        /// Символ валюты, как RUB
        /// </summary>
        public string CharCode { get; set; }
        /// <summary>
        /// Кратность валюты
        /// </summary>
        [JsonProperty("Nominal")]
        public int Nominal;
        /// <summary>
        /// Название
        /// </summary>
        [JsonProperty("Name")]
        public string Name { get; set; }
        /// <summary>
        /// Курс валюты
        /// </summary>
        [JsonProperty("Value")]
        public double Value;
        /// <summary>
        /// Предыдущий курс
        /// </summary>
        [JsonProperty("Previous")]
        public double Previous;
    }
}
