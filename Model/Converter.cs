﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// Конвертер
    /// </summary>
    public class Converter
    {
        /// <summary>
        /// Конвертация
        /// </summary>
        /// <param name="newCount">новое количество  валюты, которое конвертируется</param>
        /// <param name="nominal1"></param>
        /// <param name="nominal2"></param>
        /// <param name="valute1">курс валюты 1</param>
        /// <param name="valute2">курс валюты 2</param>
        /// <returns>вовращет новое количество купюр в валюте 2</returns>
        public double Conversion(double newCount, int nominal1, int nominal2, double kursValute1, double kursValute2)
        {
            return Math.Round(newCount * ((nominal2 * kursValute1) / (nominal1 * kursValute2)), 2);
        }
    }
}
