﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using ViewModel;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace View
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class SelectValute : Page
    {
        public SelectValute()
        {
            this.InitializeComponent();
            Windows.UI.ViewManagement.ApplicationView appView = Windows.UI.ViewManagement.ApplicationView.GetForCurrentView();
            appView.SetPreferredMinSize(new Size(300, 250));
            CustomizeTitleBar();
        }

        private void CustomizeTitleBar()
        {
            CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = true;
            Window.Current.SetTitleBar(customTitleBar);
        }

        ConverterViewModel converterViewModel;
        SelectValuteViewModel selectValuteViewModel;
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null)
            {
                converterViewModel = (ConverterViewModel)e.Parameter;
                selectValuteViewModel = new SelectValuteViewModel(converterViewModel);
                selectValuteViewModel.OpenWindowConverter += NextWindow;
                DataContext = selectValuteViewModel;
            }
        }

        /// <summary>
        /// Открытие окна конвертера
        /// </summary>
        public void NextWindow()
        {
            //converterViewModel.SelectValute1 = selectValuteViewModel.SelectedItem.CharCode;
            Frame.Navigate(typeof(ConverterView), selectValuteViewModel.ConverterViewModel);
        }
    }
}
