﻿using ViewModel;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace View
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class ConverterView : Page
    {
        public ConverterView()
        {
            this.InitializeComponent();
            Windows.UI.ViewManagement.ApplicationView appView = Windows.UI.ViewManagement.ApplicationView.GetForCurrentView();
            appView.SetPreferredMinSize(new Size(300, 250));
            CustomizeTitleBar();
        }

        private void CustomizeTitleBar()
        {
            CoreApplication.GetCurrentView().TitleBar.ExtendViewIntoTitleBar = true;
            Window.Current.SetTitleBar(customTitleBar);
        }

        DownloadViewModel downloadViewModel;
        ConverterViewModel converterViewModel;

        /// <summary>
        /// Открытие окна после, загрузки 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null)
            {
                if (e.Parameter is DownloadViewModel == true)
                {
                    downloadViewModel = (DownloadViewModel)e.Parameter;
                    converterViewModel = new ConverterViewModel(downloadViewModel);
                    converterViewModel.OpenWindowSelectValute += NextWindow;
                    DataContext = converterViewModel;
                }
                if (e.Parameter is ConverterViewModel == true)
                {
                    converterViewModel = (ConverterViewModel)e.Parameter;
                    converterViewModel.OpenWindowSelectValute += NextWindow;
                    DataContext = converterViewModel;
                }
            }
        }

        /// <summary>
        /// Открытие окна выбора валюты
        /// </summary>
        public void NextWindow(string numberValute)
        {
            if (numberValute == "Left")
            {
                converterViewModel.SymvolValute1 = numberValute;
                converterViewModel.SymvolValute2 = "";
            }
            else
            {
                converterViewModel.SymvolValute1 = "";
                converterViewModel.SymvolValute2 = numberValute;
            }
            Frame.Navigate(typeof(SelectValute), converterViewModel);
        }
    }
}