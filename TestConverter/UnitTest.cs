﻿
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestConverter
{
    [TestClass]
    public class UnitTest1
    {
        public Model.Converter converter;
        public UnitTest1()
        {
            converter = new Model.Converter();
        }

        [TestMethod]
        public void TestRub()
        {
            // RUB => EUR
            double newCount = 5; //RUB
            int nominal1 = 1; //RUB
            double valute1 = 1; //RUB
            int nominal2 = 1; //EUR
            double valute2 = 92.12; //EUR
            Assert.AreEqual(0.05, converter.Conversion(newCount, nominal1, nominal2, valute1, valute2));

            // RUB => USD
            newCount = 5; //RUB
            nominal1 = 1; //RUB
            valute1 = 1; //RUB
            nominal2 = 1; //USD
            valute2 = 78.46; //USD
            Assert.AreEqual(0.06, converter.Conversion(newCount, nominal1, nominal2, valute1, valute2));
        }

        [TestMethod]
        public void TestNoRubNominal_1()
        {
            // USD => EUR
            double newCount = 5; //USD
            int nominal1 = 1; //USD
            double kursValute1 = 78.46; //USD
            int nominal2 = 1; //EUR
            double kursValute2 = 92.12; //EUR
            Assert.AreEqual(4.26, converter.Conversion(newCount, nominal1, nominal2, kursValute1, kursValute2));

            // CAD => CNY
            newCount = 5; //CAD
            nominal1 = 1; //CAD
            kursValute1 = 59.8033; //CAD
            nominal2 = 1; //CNY
            kursValute2 = 11.8146; //CNY
            Assert.AreEqual(25.31, converter.Conversion(newCount, nominal1, nominal2, kursValute1, kursValute2));

            // CNY => CAD
            newCount = 5; //CNY
            nominal1 = 1; //CNY
            kursValute1 = 11.8146; //CNY
            nominal2 = 1; //CAD
            kursValute2 = 59.8033; //CAD
            Assert.AreEqual(0.99, converter.Conversion(newCount, nominal1, nominal2, kursValute1, kursValute2));
        }

        [TestMethod]
        public void TestNoRubNominal_100()
        {
            // AMD => EUR
            double newCount = 5; //AMD
            int nominal1 = 100; //AMD
            double kursValute1 = 15.893; //AMD
            int nominal2 = 1; //EUR
            double kursValute2 = 92.12; //EUR
            Assert.AreEqual(0.01, converter.Conversion(newCount, nominal1, nominal2, kursValute1, kursValute2));

            // EUR => AMD
            newCount = 5; //EUR
            nominal1 = 1; //EUR
            kursValute1 = 92.1229; //EUR
            nominal2 = 100; //AMD
            kursValute2 = 15.893; //AMD
            Assert.AreEqual(2898.22, converter.Conversion(newCount, nominal1, nominal2, kursValute1, kursValute2));
        }
    }
}