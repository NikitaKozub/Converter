﻿using Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    /// <summary>
    /// ViewModel окна SelectView
    /// </summary>
    public class SelectValuteViewModel : ApplicationViewModel
    {
        public ConverterViewModel ConverterViewModel;
       
        public SelectValuteViewModel(ConverterViewModel converterViewModel)
        {
            ConverterViewModel = converterViewModel;
            ListValute = ConverterViewModel.DownloadViewModel.DataValutes.Valute.Values.ToList();
            if(ConverterViewModel.SymvolValute1 == "Left"){
                for (int i = 0; i < ListValute.Count; i++)
                {
                    if(ListValute[i].CharCode == ConverterViewModel.SymValute1)
                    {
                        SelectedItem = ListValute[i];
                        OnPropertyChanged("ListValute");
                        ConverterViewModel.SymvolValute1 = "+";
                    }
                }
            }
            else
            {
                for (int i = 0; i < ListValute.Count; i++)
                {
                    if (ListValute[i].CharCode == ConverterViewModel.SymValute2)
                    {
                        SelectedItem = ListValute[i];
                        OnPropertyChanged("ListValute");
                        ConverterViewModel.SymvolValute2 = "+";
                    }
                }
            }
            
        }

        private List<Valute> listValute;
        /// <summary>
        /// Список все валют
        /// </summary>
        public List<Valute> ListValute
        {
            get 
            {
                return listValute; 
            }
            set
            {
                listValute = value;
                OnPropertyChanged("ListValute");
            }
        }

        public delegate void MethodOpenWindowConverter();
        public event MethodOpenWindowConverter OpenWindowConverter;

        private Valute _selectedItem;
        /// <summary>
        /// ВЫбранная валюты
        /// </summary>
        public Valute SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem != value)
                {
                    _selectedItem = value;
                    if (ConverterViewModel.SymvolValute1 == "+" ||
                        ConverterViewModel.SymvolValute2 == "+")
                    {
                        if (ConverterViewModel.SymvolValute1 == "+")
                        {
                            ConverterViewModel.SymValute1 = _selectedItem.CharCode;
                        }
                        if (ConverterViewModel.SymvolValute2 == "+")
                        {
                            ConverterViewModel.SymValute2 = _selectedItem.CharCode;
                        }
                        OpenWindowConverter();
                    }
                    //OnPropertyChanged("SelectedItem");
                }
            }
        }
    }
}
