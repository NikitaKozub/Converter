﻿using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel
{
    /// <summary>
    ///  ViewModel окна DownloadView
    /// </summary>
    public class DownloadViewModel : ApplicationViewModel
    {
        public DownloadViewModel()
        {
            StartConverter();
        }

        public void StartConverter()
        {
            DownloadData();
        }

        /// <summary>
        /// Запуск скачки данных
        /// </summary>
        public void DownloadData()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += WorkerModification;
            worker.ProgressChanged += WorkerProgressModification;
            worker.RunWorkerCompleted += WorkerRunWorkerCompleted;
            worker.RunWorkerAsync(10000);
        }

        void WorkerModification(object sender, DoWorkEventArgs e)
        {
            int progress = 0;
            (sender as BackgroundWorker).ReportProgress(progress, 0);

            ParserJson parser = new ParserJson();
            DataValutes = parser.GetData();

            progress = 1;
            (sender as BackgroundWorker).ReportProgress(progress);
            e.Result = false;
        }

        /// <summary>
        /// Обновления
        /// </summary>
        void WorkerProgressModification(object sender, ProgressChangedEventArgs e)
        {
            IsDownload = true;
        }

        public delegate void MethodOpenWindowConverter();
        public event MethodOpenWindowConverter OpenWindowConverter;
        /// <summary>
        /// Закончилась загрузка
        /// </summary>
        void WorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            IsDownload = (bool)e.Result;
            DailyValutes_ = DataValutes;
            OpenWindowConverter();
        }


        private bool isDownload;
        /// <summary>
        /// Завершена ли загрузка файла
        /// </summary>
        public bool IsDownload
        {
            get { return isDownload; }
            set
            {
                isDownload = value;
                OnPropertyChanged("IsDownload");
            }
        }

        private DailyValutes dailyValutes_;
        public DailyValutes DailyValutes_
        {
            get { return dailyValutes_; }
            set
            {
                dailyValutes_ = value;
                OnPropertyChanged("DailyValutes_");
            }
        }
    }
}
