﻿using System.Linq;

namespace ViewModel
{
    /// <summary>
    /// ViewModel окна ConverterView
    /// </summary>
    public class ConverterViewModel : ApplicationViewModel
    {
        public DownloadViewModel DownloadViewModel;

        Model.Converter converter;
        public ConverterViewModel(DownloadViewModel downloadViewModel)
        {
            DownloadViewModel = downloadViewModel;
            converter = new Model.Converter();
            Valute1 = 1;
            if (downloadViewModel.SymvolValute1 == null)
            {
                SymValute1 = "RUB";
            }
            else
            {
                if (SymValute1 != downloadViewModel.SymvolValute1)
                {
                    SymValute1 = downloadViewModel.SymvolValute1;
                }
            }
            if (downloadViewModel.SymvolValute2 == null)
            {
                string keyAnyElement = downloadViewModel.DailyValutes_.Valute.First().Key;
                Valute2 = downloadViewModel.DailyValutes_.Valute[keyAnyElement].Value;
                SymValute2 = downloadViewModel.DailyValutes_.Valute[keyAnyElement].CharCode;
            }
            else
            {
                if (SymValute2 != downloadViewModel.SymvolValute2)
                {
                    Valute2 = downloadViewModel.SummValute2;
                    SymValute2 = downloadViewModel.SymvolValute2;
                }
            }
        }

        private double valute1;
        /// <summary>
        /// Количество валюты 1
        /// </summary>
        public double Valute1
        {
            get { return valute1; }
            set
            {
                if (value <= 0)
                {
                    valute1 = 1;
                }
                else
                {
                    valute1 = value;
                }
                if (SymValute2 != null && SymValute1 != null)
                {
                    valute2 = converter.Conversion(valute1, DownloadViewModel.DailyValutes_.Valute[SymValute1].Nominal, DownloadViewModel.DailyValutes_.Valute[SymValute2].Nominal,
                                                   DownloadViewModel.DailyValutes_.Valute[SymValute1].Value, DownloadViewModel.DailyValutes_.Valute[SymValute2].Value);
                    OnPropertyChanged("Valute2");
                }
                else
                {
                    OnPropertyChanged("Valute1");
                }
            }
        }

        private string symValute1;
        /// <summary>
        /// Символ валюты 1
        /// </summary>
        public string SymValute1
        {
            get { return symValute1; }
            set
            {
                symValute1 = value;
                OnPropertyChanged("SymValute1");
                Valute1 = valute1;//
            }
        }

        private double valute2;
        /// <summary>
        /// Количество валюты 2
        /// </summary>
        public double Valute2
        {
            get { return valute2; }
            set
            {
                if (value <= 0)
                {
                    valute2 = 1;
                }
                else
                {
                    valute2 = value;
                }
                valute2 = value;
                if (SymValute2 != null && SymValute1 != null)
                {
                    valute1 = converter.Conversion(valute2, DownloadViewModel.DailyValutes_.Valute[SymValute1].Nominal, DownloadViewModel.DailyValutes_.Valute[SymValute2].Nominal,
                                                   DownloadViewModel.DailyValutes_.Valute[SymValute2].Value, DownloadViewModel.DailyValutes_.Valute[SymValute1].Value);
                    OnPropertyChanged("Valute1");
                }
                else
                {
                    OnPropertyChanged("Valute2");
                }
            }
        }

        private string symValute2;
        /// <summary>
        /// Символ валюты 2
        /// </summary>
        public string SymValute2
        {
            get {
                return symValute2; 
            }
            set
            {
                symValute2 = value;
                OnPropertyChanged("SymValute2");
                Valute2 = valute2;//
            }
        }

        public delegate void MethodOpenWindowSelect(string numberValute);
        public event MethodOpenWindowSelect OpenWindowSelectValute;

        DelegateCommand2 _EditPersonCommand;
        /// <summary>
        /// Кнопка смены валюты
        /// </summary>
        public DelegateCommand2 SelectValute
        {
            get
            {
                return _EditPersonCommand ??
                    (_EditPersonCommand = new DelegateCommand2((selectedItem) =>
                    {
                        if (_EditPersonCommand != null)
                        {
                            OpenWindowSelectValute(selectedItem.ToString()); 
                        }
                    }));
            }
        }
    }
}
